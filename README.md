# Piove oggi a Reggio Emilia?

## What does it do?

Tells you if it's / it's going to rain in Reggio Emilia.

![Screenshot](https://i.imgur.com/Lv1VFkW.jpg)

## Setup

- Create a `env.js` file containing `API_TOKEN="yourtoken"`, take a look at env.template.js (It's a workaround for .env, I couldn't make `react-native-dotenv` work).

## Built With

- [React Native](https://reactnative.dev/) - Create native apps for Android and iOS using React.
- [Expo.io](https://expo.io/) - A framework and a platform for universal React applications.
- [WeatherAPI](https://www.weatherapi.com//) - JSON and XML Weather API and Geolocation Developer API.

### Credits

Inspired by [pioveoggiaroma.it](https://pioveoggiaroma.it/)
