import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { API_TOKEN } from './env';

export default function App() {
    const [data, setData] = useState({
        nome: '',
        oggi: '',
        domani: '',
    });
    const [isLoading, setLoading] = useState(null);

    const fetchData = () => {
        return fetch(`http://api.weatherapi.com/v1/forecast.json?key=${API_TOKEN}&q=Reggio%20nell'Emilia&aqi=no&alerts=no&days=2`)
            .then((response) => response.json())
            .then((json) => {
                setData({
                    nome: "Reggio Emilia", // json.location.name
                    oggi: json.current.condition.text.toLowerCase(),
                    domani: json.forecast.forecastday[1].day.daily_chance_of_rain,
                });
                // console.log(data);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    useEffect(() => {
        setLoading(true);
        fetchData();
    }, []);

    const getCurrentStatus = (condition) => {
        if (condition.includes('rain')) {
            return 'Si';
        } else {
            return 'No';
        }
    };

    const getTomorrowStatus = (percentage) => {
        if (percentage < 10) {
            return 'No';
        } else if (percentage <= 30) {
            return 'Non penso';
        } else if (percentage > 30 && percentage <= 60) {
            return 'Forse';
        } else if (percentage > 60 && percentage <= 80) {
            return 'Potrebbe';
        } else if (percentage > 80 && percentage <= 90) {
            return 'Molto probabile';
        } else {
            if (getCurrentStatus('rain') == 'Si') {
                return 'Pure';
            } else {
                return 'Sicuramente';
            }
        }
    };

    return (
        <View style={styles.container}>
            <StatusBar style="auto" />
            {isLoading ? (
                <ActivityIndicator size="large" color="#4ECDC4" />
            ) : (
                <>
                    <Text style={styles.text}>Piove oggi a</Text>
                    <Text style={styles.city}>{data.nome}</Text>
                    <Text style={styles.today}>{getCurrentStatus(data.oggi)}</Text>
                    <Text style={styles.text}>e domani?</Text>
                    <Text style={styles.tomorrow}>{getTomorrowStatus(data.domani)}</Text>
                </>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#292F36',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
        // fontWeight: 'bold',
        color: '#FFFFFF',
    },
    city: {
        textAlign: 'center',
        fontSize: 45,
        // fontWeight: 'bold',
        color: '#FF6B6B',
    },
    today: {
        textAlign: 'center',
        fontSize: 150,
        // fontWeight: 'bold',
        color: '#4ECDC4',
        marginVertical: 10,
    },
    tomorrow: {
        textAlign: 'center',
        fontSize: 75,
        // fontWeight: 'bold',
        color: '#4ECDC4',
        marginVertical: 20,
    },
});
